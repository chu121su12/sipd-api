<div style="
    display: flex;
    left: 0;
    position: fixed;
    top: 1.5rem;
    width: 100%;
">
    <div style="
        background-color: white;
        display: flex;
        justify-content: space-between;
        margin: 0 1rem;
        padding: 0.5rem;
        width: 100%;
        box-shadow:
            0 1px 3px 0 rgba(0, 0, 0, 0.1),
            0 1px 2px 0 rgba(0, 0, 0, 0.06);
    ">
        <span>Divisi TiOps</span>
        <span>–</span>
        <span>SIPD API</span>
        <span>–</span>
        <span>Jumat, 23 Oktober 2020</span>
    </div>
</div>
<br>

<img title="BSG" src="https://portal.banksulutgo.co.id/asset/branding-2020/master.svg" width="200">

# Daftar Isi
- [Pendahuluan](#pendahuluan)
- [Persyaratan](#persyaratan)
- [1. Overbooking](#1-overbooking)
- [2. Status Check](#2-status-check)
- [3. Inquiry](#3-inquiry)
- [4. Transaction History](#4-transaction-history)
- [5. Transaction Callback API](#5-transaction-callback-api)
- [Lainnya](#lainnya)

# SIPD API

## Pendahuluan

SIPD API adalah perangkat lunak berbentuk servis yang bertindak sebagai perantara untuk komunikasi antara Sistem SIPD Asbanda dan Sistem Core Banking BSG. Ini merupakan penghubung yang terpercaya untuk mengakses Sistem Core Banking BSG tanpa perlu mengakses langsung ke database bank.

SIPD API menyediakan API yang diperlukan untuk Sistem SIPD Asbanda. Pihak Asbanda dapat melakukan transkasi dan mendapatkan informasi dengan mengirimkan permintaan lewat API. Dokumen ini menyediakan detil dari bagaimana cara menggunakan SIPD API menggunakan HTTP dan JSON, beserta informasi yang dibutuhkan.

## Persyaratan
SIPD API dapat diakses melalui web service dengan mengakses url https://123.231.247.2/sipd/ dan menyertakan API KEY (SecretKey) pada header http. Fitur dari SIPD API ini berupa:

1. Overbooking
2. Status Check
3. Inquiry
4. Transaction History
5. Transaction Callback API

### 1. Overbooking
- Fungsi: Untuk melakukan request transaksi ke SIPD API
- URL: `POST` https://123.231.247.2/sipd/overbooking
- Contoh request:

    ```http
    POST /sipd/overbooking HTTP/1.1
    Host: 123.231.247.2
    Content-type: application/json
    Authorization: SecretKey ###-KEY-###

    {
        "partnerId": "string",
        "userId": "string",
        "totalAmount": "3000",
        "senderInfo": {
            "accountBankCode": "string",
            "accountNumber": "string",
            "amount": "3000",
            "note": "string",
            "sp2dSpjNumber": "string (opsional)",
            "sp2dSpjDesc": "string (opsional)",
            "skpd": "string",
            "npwpBendahara": "string"
        },
        "recipientInfo": {
            "accountBankCode": "string",
            "accountNumber": "string",
            "accountName": "string",
            "txId": "string",
            "amount": "2000",
            "npwpRekanan": "string",
            "potongan": [
                {
                    "txId": "string",
                    "amount": "1000",
                    "nomorObjekPajak": "string (opsional)",
                    "mataAnggaran": "string (opsional)",
                    "jenisSetoran": "string (opsional)"
                }
            ]
        }
    }
    ```

- Keterangan:

    | Parameter | Tipe | Keterangan |
    | - | - | - |
    | `partnerId` | `string(32)` | ID yang unik (*static*) yang sudah disetujui yang akan dikirim dari SIPD Asbanda sebagai identifier/bukti tujuan transaksi ke BSG.
    | `userId` | `string(5)` | ID user pengguna SIPD Asbanda yang melakukan approve/release transaksi.
    | `totalAmount` | `string` | Total nominal yang akan ditransaksikan. Untuk verifikasi total transaksi dan potongan. Apabila nilai tidak sesuai, request akan ditolak.
    | `senderInfo` | `json key-value object` | Data pengirim transaksi.
    | `recipientInfo` | `json key-value object` | Data tujuan transaksi.
    | `accountBankCode` | `string(3)` | Kode bank sesuai ATMB.
    ||| - Untuk `senderInfo` kode ini harus selalu diisi `127`.
    ||| - Untuk `recipientInfo` kode diisi sesuai kode bank yang dituju.
    | `accountNumber` | `string(20)` | Nomor rekening bank.
    | `accountName` | `string(255)` | Nama pemilik rekening.
    | `amount` | `string` | Nominal yang akan ditransaksikan.
    ||| - Untuk `senderInfo` nominal tersebut akan didebet dari rekening pengirim.
    ||| - Untuk `recipientInfo` nominal tersebut adalah nominal yang akan dikirimkan ke rekening tujuan, termasuk biaya yang mungkin dibebankan.
    ||| - Untuk `potongan` nominal tersebut berupa nominal yang akan dibayarkan.
    | `note` | `string(255)` | Keterangan/uraian.
    | `skpd` | `string(255)` | Kode unik SKPD. Ini dimaksudkan agar di sisi BSG dapat melakukan rekonsiliasi dengan SKPD.
    | `sp2dSpjNumber` | `string(255)`, *opsional* | Isian nomor SP2D/Bukti transaksi.
    | `sp2dSpjDesc` | `string(255)`, *opsional* | Isian keperluan kegiatan/uraian SP2D/Bukti transaksi.
    | `npwpBendahara` | `string(255)` | NPWP bendahara.
    | `npwpRekanan` | `string(255)` | NPWP rekanan.
    | `txId` | `string(32)` | ID transaksi yang unik yang akan dikirim dari SIPD Asbanda. ID ini berbeda untuk setiap `recipientInfo` dan `potongan`.
    | `potongan` | `json array` | Daftar potongan. Kosongkan (`json array` yang kosong, `[]`) jika transaksi ini tanpa potongan. Potongan dapat diisi lebih dari 1 potongan, sesuai kebutuhan transaksi SP2D/Bukti.
    | `nomorObjekPajak` | `string(255)`, *opsional* | Nomor objek pajak.
    | `mataAnggaran` | `string(255)`, *opsional* | Mata anggaran. Jika tidak diisi, potongan dianggap potongan lainnya (non-MPN).
    | `jenisSetoran` | `string(255)`, *opsional* | Jenis setoran.

- Contoh response berhasil:

    ```http
    HTTP/1.1 200 OK
    Content-type: application/json

    {
        "partnerId": "string",
        "status": {
            "code": "string",
            "message": "string"
        },
        "data": [
            {
                "txId": "string",
                "created": "string",
                "updatedLocal": "string",
                "code": "string",
                "message": "string"
            }
        ]
    }
    ```

- Keterangan:

    | Parameter | Tipe | Keterangan |
    | - | - | - |
    | `partnerId` | `string(32)` | ID unik penanda transaksi dari BSG.
    | `status` | `json key-value object` | Status pesan balasan.
    | `data` | `json key-value object` | Daftar transaksi yang dilakukan/akan dilakukan.
    | `code` | `string(3)` | Detil response code sesuai spesifikasi SIPD Asbanda.
    | `message` | `string(255)` | Pesan response code sesuai spesifikasi SIPD Asbanda.
    | `txId` | `string(32)` | ID transaksi sesuai request.
    | `created` | `string`, sesuai keterangan | Tanggal transaksi dibentuk dengan format `dd-MM-YYYY HH:mm:ss` pada UTC+7.
    | `updatedLocal` | `string`, sesuai keterangan | Tanggal transaksi diupdate dengan format `dd-MM-YYYY HH:mm:ss` pada UTC+8.

- Contoh response gagal:

    ```http
    HTTP/1.1 400 Bad Request
    Content-type: application/json

    {
        "partnerId": "string",
        "status": {
            "code": "string",
            "message": "string"
        }
    }
    ```

- Keterangan:

    | Parameter | Tipe | Keterangan |
    | - | - | - |
    | `partnerId` | `string(32)` | ID unik penanda transaksi dari BSG.
    | `status` | `json key-value object` | Status pesan balasan.
    | `code` | `string(3)` | Detil response code sesuai spesifikasi SIPD Asbanda.
    | `message` | `string(255)` | Pesan response code sesuai spesifikasi SIPD Asbanda.

### 2. Status Check
- Fungsi: Untuk melakukan cek status transaksi
- URL: `POST` https://123.231.247.2/sipd/status-check
- Contoh request:

    ```http
    POST /sipd/status-check HTTP/1.1
    Host: 123.231.247.2
    Content-type: application/json
    Authorization: SecretKey ###-KEY-###

    {
        "partnerId": "string",
        "userId": "string",
        "txId": "string"
    }
    ```

- Keterangan:

    | Parameter | Tipe | Keterangan |
    | - | - | - |
    | `partnerId` || Cek api #1.
    | `userId` || Cek api #1.
    | `txId` | `string(32)` | ID transaksi yang diperlukan (cek api #1).

- Contoh response berhasil:

    ```http
    HTTP/1.1 200 OK
    Content-type: application/json

    {
        "partnerId": "string",
        "status": {
              "txId": "string",
              "created": "string",
              "updatedLocal": "string",
              "code": "string",
              "message": "string"
          }
    }
    ```

- Keterangan:

    | Parameter | Tipe | Keterangan |
    | - | - | - |
    | `partnerId` || Cek api #1.
    | `status` | `json key-value object` | Status pesan balasan beserta data transaksi yang diminta (cek keterangan response `data` di api #1).
    | `created` || Cek api #1.
    | `updatedLocal` || Cek api #1.
    | `code` || Cek api #1.
    | `message` || Cek api #1.

- Contoh response gagal: Cek api #1.

### 3. Inquiry
- Fungsi: Untuk mengambil nama pemilik rekening
- URL: `POST` https://123.231.247.2/sipd/inquiry
- Contoh request:

    ```http
    POST /sipd/inquiry HTTP/1.1
    Host: 123.231.247.2
    Content-type: application/json
    Authorization: SecretKey ###-KEY-###

    {
        "partnerId": "string",
        "userId": "string",
        "accountBankCode": "string",
        "accountNumber": "string"
    }
    ```

- Keterangan:

    | Parameter | Tipe | Keterangan |
    | - | - | - |
    | `partnerId` || Cek api #1.
    | `userId` || Cek api #1.
    | `accountBankCode` || Cek api #1.
    | `accountNumber` || Cek api #1.

- Contoh response berhasil:

    ```http
    HTTP/1.1 200 OK
    Content-type: application/json

    {
        "partnerId": "string",
        "accountName": "string",
        "status": {
            "code": "string",
            "message": "string"
        }
    }
    ```

- Keterangan:

    | Parameter | Tipe | Keterangan |
    | - | - | - |
    | `partnerId` || Cek api #1.
    | `accountName` || Cek api #1.
    | `status` || Cek api #1.
    | `code` || Cek api #1.
    | `message` || Cek api #1.

- Contoh response gagal: Cek api #1.

### 4. Transaction History
- Fungsi: Untuk melakukan cek status transaksi berdasarkan tanggal
- URL: `POST` https://123.231.247.2/sipd/transaction-history
- Contoh request:

    ```http
    POST /sipd/transaction-history HTTP/1.1
    Host: 123.231.247.2
    Content-type: application/json
    Authorization: SecretKey ###-KEY-###

    {
        "partnerId": "string",
        "userId": "string",
        "createdDateStart": "string",
        "createdDateEnd": "string",
        "limit": 1,
        "offset": 0,
        "sort": "asc/desc"
    }
    ```

- Keterangan:

    | Parameter | Tipe | Keterangan |
    | - | - | - |
    | `partnerId` || Cek api #1.
    | `userId` || Cek api #1.
    | `createdDateStart` | `string`, sesuai keterangan | Tanggal awal pencarian terhadap tanggal transaksi dibentuk dengan format `dd-MM-YYYY HH:mm:ss` pada UTC+7.
    | `createdDateEnd` | `string`, sesuai keterangan | Tanggal akhir pencarian terhadap tanggal transaksi dibentuk dengan format `dd-MM-YYYY HH:mm:ss` pada UTC+7.
    | `limit` | `integer` | Jumlah transaksi yang diminta.
    | `offset` | `integer` | Offset.
    | `sort` | `string`, sesuai keterangan | Urutan data yang diminta, dengan isian `asc` untuk urutan naik dan `desc` untuk urutan menurun.

- Contoh response berhasil:

    ```http
    HTTP/1.1 200 OK
    Content-type: application/json

    {
        "partnerId": "string",
        "status": {
            "code": "string",
            "message": "string"
        },
        "data": [
            {
                "txId": "string",
                "created": "string",
                "updatedLocal": "string",
                "code": "string",
                "message": "string"
            }
        ],
        "totalCount": 2
    }
    ```

- Keterangan:

    | Parameter | Tipe | Keterangan |
    | - | - | - |
    | `partnerId` || Cek api #1.
    | `status` || Cek api #1.
    | `data` || Cek api #1.
    | `totalCount` | `integer` | Jumlah data yang dimnta.
    | `code` || Cek api #1.
    | `message` || Cek api #1.
    | `txId` || Cek api #1.
    | `created` || Cek api #1.
    | `updatedLocal` || Cek api #1.

- Contoh response gagal: Cek api #1.

### 5. Transaction Callback API
- Fungsi: Untuk menginformasikan perubahan status `code` dari pihak BSG ke sistem SIPD Asbanda.
- URL: `POST` #url# Menunggu kesepakatan.
- Contoh request:

    ```http
    POST #url# HTTP/1.1
    Host: #ip url#
    Authorization: SecretKey ###-KEY-###

    {
        "partnerId": "string",
        "status": {
            "txId": "string",
            "created": "string",
            "updatedLocal": "string",
            "code": "string",
            "message": "string"
        }
    }
    ```

- Keterangan:

    | Parameter | Tipe | Keterangan |
    | - | - | - |
    | `partnerId` || Cek api #1.
    | `status` || Cek api #2.
    | `txId` || Cek api #1.
    | `created` || Cek api #1.
    | `updatedLocal` || Cek api #1.
    | `code` || Cek api #1.
    | `message` || Cek api #1.

- Contoh response berhasil:

    ```http
    HTTP/1.1 200 OK
    Content-type: application/json

    {
        "partnerId": "string",
        "status": {
            "code": "string",
            "message": "string"
        }
    }
    ```

- Keterangan:

    | Parameter | Tipe | Keterangan |
    | - | - | - |
    | `partnerId` || Cek api #1.
    | `status` || Cek api #1.
    | `code` || Cek api #1.
    | `message` || Cek api #1.

- Contoh response gagal:

    ```http
    HTTP/1.1 400 Bad Request
    Content-type: application/json

    {
        "partnerId": "string",
        "status": {
            "code": "string",
            "message": "string"
        }
    }
    ```

- Keterangan:

    | Parameter | Tipe | Keterangan |
    | - | - | - |
    | `partnerId` || Cek api #1.
    | `status` || Cek api #1.
    | `code` || Cek api #1.
    | `message` || Cek api #1.

## Lainnya
- Untuk header `Authorization`, token `###-KEY-###` diganti dengan key yang sudah disetujui.
- Masing-masing parameter bersifat **mandatory/wajib**, kecuali ditandai *opsional*.
- Untuk parameter yang ditandai *opsional*, tetap dikirimkan dengan isian string kosong `""`.
- Nominal berupa `string` dengan format desimal menggunakan `[.]`. Pemisah desimal `[,]` dianggap salah. Pemisah angka bulat `[.]` dianggap salah dan dapat mengubah nilai nominal.
